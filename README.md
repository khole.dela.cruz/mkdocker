# MkDockerize
## Dockerfile Notes
- created shell scripts for produce and serve commands
- mounts the specified directory to /tmp/mounted-project
- produce creates the **output.tar.gz** in the specified directory's root
- if nothing is mounted, error occurs


## Wrapper Script Notes
- run using command line arguments -m/--mount and -i/--imagename
- Run using:
```bash
$ ./mkdockerize.sh -m $(pwd)/sample-project -i mkdocker produce
$ ./mkdockerize.sh -m $(pwd)/sample-project -i mkdocker serve
```

## Gitlab CI Notes
- there are three stages (build, test, push)
- **build:** creates a temp image and pushes it to the repository
- **test:** pulls test images and runs the wrapper script for the corresponding test
- **test-produce:** checks if **output.tar.gz** exists in the project's root
- **test-serve:** does a curl to see if site is accessible
- **push:** tags the temp image to latest and pushes it to the repository

## TODO
- catch errors in mkdockerize.sh if arguments are lacking
- do man page for mkdockerize.sh