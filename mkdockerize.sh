#!/bin/bash

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -m|--mount)
    MOUNT="$2"
    shift # past argument
    shift # past value
    ;;
    -i|--imagename)
    IMAGENAME="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

echo MOUNT       = "${MOUNT}"
echo IMAGENAME   = "${IMAGENAME}"

docker build -t ${IMAGENAME} .
if [ $1 == 'produce' ]; then
    docker run -it --mount src=${MOUNT},target=/tmp/mounted-project,type=bind ${IMAGENAME} produce
elif [ $1 == 'serve' ]; then
    docker run -it --mount src=${MOUNT},target=/tmp/mounted-project,type=bind -p 8000:8000 ${IMAGENAME} serve
fi