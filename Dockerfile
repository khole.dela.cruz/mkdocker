FROM python:3.6.8-alpine3.8
RUN apk add --no-cache bash && \
    pip install mkdocs

RUN cd /usr/local/bin/ && \
    echo 'cd /tmp/mounted-project && mkdocs serve --dev-addr=0.0.0.0:8000' > serve && \
    chmod +x serve

RUN cd /usr/local/bin/ && \
    echo 'cd /tmp/mounted-project && mkdocs build && tar -C /tmp/mounted-project/site -czvf output.tar.gz . && rm -rf /tmp/mounted-project/site' > produce && \
    chmod +x produce

ENTRYPOINT ["bash"]